<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 * @ApiResource(
 *     normalizationContext={"groups"={"address:read"}},
 *     denormalizationContext={"groups"={"address:write"}}
 * )
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 * @ORM\EntityListeners({"App\EventListener\AddressOwnerListener"})
 */
class Address
{
    /**
     * @var int
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     *
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     * @ORM\Column(name="address_type", type="string", length=30, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max= 10
     * )
     *
     */
    private $addressType;

    /**
     * @var string|null
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     * @ORM\Column(name="street", type="string", length=256, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max= 100
     * )
     */
    private $street;

    /**
     * @var string|null
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     * @ORM\Column(name="houseNr", type="string", length=256, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max= 5
     * )
     */
    private $housenr;

    /**
     * @var string|null
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"}))
     * @ORM\Column(name="busnr", type="string", length=256, nullable=true)
     * @Assert\Length(
     *     max= 5
     * )
     */
    private $busnr;



    /**
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     *  @ORM\ManyToOne(targetEntity=User::class, inversedBy="addresses")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="addresses")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"address:read","address:write","user:read","user:write","admin:write"})
     */
    private $city;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddressType(): ?string
    {
        return $this->addressType;
    }

    public function setAddressType(?string $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHousenr(): ?string
    {
        return $this->housenr;
    }

    public function setHousenr(?string $housenr): self
    {
        $this->housenr = $housenr;

        return $this;
    }

    public function getBusnr(): ?string
    {
        return $this->busnr;
    }

    public function setBusnr(?string $busnr): self
    {
        $this->busnr = $busnr;

        return $this;
    }



    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->city." ".$this->street;
    }


}

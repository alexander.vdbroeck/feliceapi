<?php


namespace App\Extentions;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use App\Entity\ProductPrice;
use Symfony\Component\Security\Core\Security;

class ActivePriceExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{

    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {

        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->getActivePriceExtention($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        try {
            $this->getActivePriceExtention($queryBuilder, $resourceClass);
        } catch (\Exception $e) {
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $resourceClass
     * @throws \Exception
     */
    public function getActivePriceExtention(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        // als er een admin gebruiker prijzen opzoekt via een api endpoint zal hij alle prijzen te zien krijgen,
        // gewone user krijgen slechts actieve prijzen te zien

        if($this->security->isGranted("ROLE_ADMIN")){
            return;
        }
        if ($resourceClass !== ProductPrice::class) {
            return;
        }
        return;
        $rootAlias = $queryBuilder->getRootAlias()[0];
        $queryBuilder->andWhere(sprintf('%s.startedAt < :date', $rootAlias))
            ->andWhere(sprintf('%s.endedAt > :date', $rootAlias))
            ->setParameter('date', new \DateTimeImmutable('now'));
    }
}


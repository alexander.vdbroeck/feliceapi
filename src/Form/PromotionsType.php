<?php

namespace App\Form;

use App\Entity\ProductPromotions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromotionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('percentage')
            ->add('fixedPrise')
            ->add('fromDate', DateType::class, array(
                'widget' => 'single_text'))
            ->add('untilDate', DateType::class, array(
                'widget' => 'single_text'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductPromotions::class]);
    }
}

<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }


    /**
     * @param PersistentCollection $prices
     * @return array
     * @throws \Exception
     */
    public static function getActivePrice(PersistentCollection $prices)
     {
         // eerst wordt er naar de active prijzen gezocht daarna wordt deze omgezet naar een array met de prijs en taxrate
         $dateTimeNow = new \DateTime();
         $criteria =  Criteria::create()
             ->andWhere(Criteria::expr()->gte('endedAt',$dateTimeNow))
             ->andWhere(Criteria::expr()->lte('startedAt',$dateTimeNow))
             ->orderBy(["id" =>"DESC"])
             ->setMaxResults(1);
         $price = $prices->matching($criteria);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if($price){
            $priceArray = $price->toArray();
            // Haalt de actieve prijs en taxrate op
            foreach ($priceArray as $item){
            $price = $propertyAccessor->getValue($item, "price");
            $taxrate = $propertyAccessor->getValue($item, "taxRate");
            if($price){
            return  ["price" =>$price, "taxrate"=> $taxrate];
            }
        }
            // als er geen actieve prijs is wordt een lege array teruggegeven
            // deze wordt in de frond end applicatie gefilterd zodoende er niet toevallig producten zijn zonder prijs
        return [];
        }
        return [];

     }

            // ter ondersteuning voor de easy admin , om het overzicht te verbeteren.
    public static function getLastPrices($prices)
    {
        $dateTimeNow = new \DateTime();
//         $dateTimeNow = $dateTime->format('Y-m-d H:i:s');
        $criteria =  Criteria::create()
            ->orderBy(["id" =>"DESC"])
            ->setMaxResults(2);
        return $prices->matching($criteria);

    }


    /**
     * @param $articelPromotions
     * @param bool $fixed
     * @return array
     * @throws \Exception
     */
    public static function getActivePromotion(PersistentCollection $articelPromotions,$fixed = false)
        {

            // eerst naar de actieve promoties zoeken
            $dateTime = new \DateTime();
            $dateTimeNow = $dateTime->format('Y-m-d H:i:s');
            $criteria =  Criteria::create()
                ->andWhere(Criteria::expr()->gte('untilDate',$dateTimeNow))
                ->andWhere(Criteria::expr()->lte('fromDate',$dateTimeNow))
                ->orderBy(["id" =>"DESC"])
                ->setMaxResults(1);
            $promotion = $articelPromotions->matching($criteria);
            $promo =[];
            if(!$promotion){
            return $promo ;
        }

            // Het object omzetten ,naar een array met enkel het type korting (vaste prijs of percentage)
            // en de oomschrijving
    $propertyAccessor = PropertyAccess::createPropertyAccessor();
    foreach ($promotion as $item) {
    if ($fixed) {
    $promo['amount'] = $propertyAccessor->getValue($item, "fixed_prise");
    if ($promo) {
    $promo['type'] = "fixed";
    $promo['discr'] = $propertyAccessor->getValue($item, "description");
    }

    } else {
        $promo['amount'] = $propertyAccessor->getValue($item, "percentage");
        if ($promo) {
            $promo['type'] = "percentage";
            $promo['discr'] = $propertyAccessor->getValue($item, "description");
        }
    }
    }
    return $promo;

    }


}

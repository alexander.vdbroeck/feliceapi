<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200517123603 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_promotions DROP FOREIGN KEY product_promotions_category_id_fk');
        $this->addSql('ALTER TABLE product_promotions DROP FOREIGN KEY product_promotions_product_id_fk');
        $this->addSql('DROP INDEX product_promotions_product_id_fk ON product_promotions');
        $this->addSql('DROP INDEX product_promotions_category_id_fk ON product_promotions');
        $this->addSql('ALTER TABLE product_promotions DROP product, DROP category');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_promotions ADD product INT DEFAULT NULL, ADD category INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_promotions ADD CONSTRAINT product_promotions_category_id_fk FOREIGN KEY (category) REFERENCES category (id)');
        $this->addSql('ALTER TABLE product_promotions ADD CONSTRAINT product_promotions_product_id_fk FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('CREATE INDEX product_promotions_product_id_fk ON product_promotions (product)');
        $this->addSql('CREATE INDEX product_promotions_category_id_fk ON product_promotions (category)');
    }
}

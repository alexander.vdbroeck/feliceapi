<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200518183107 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY product_discription_category_id_fk');
        $this->addSql('DROP TABLE discription_category');
        $this->addSql('DROP INDEX product_discription_category_id_fk ON product');
        $this->addSql('ALTER TABLE product DROP discretion_category');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discription_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(256) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, UNIQUE INDEX id (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE product ADD discretion_category INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT product_discription_category_id_fk FOREIGN KEY (discretion_category) REFERENCES discription_category (id)');
        $this->addSql('CREATE INDEX product_discription_category_id_fk ON product (discretion_category)');
    }
}

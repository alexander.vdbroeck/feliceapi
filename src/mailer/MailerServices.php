<?php


namespace App\mailer;


use App\Entity\User;
use Twig\Environment;

class MailerServices
{
    /**
     * @var \Swift_Mailer
     */
    private $swift_Mailer;
    /**
     * @var Environment
     */
    private $environment;

    public function __construct(\Swift_Mailer $swift_Mailer, Environment $environment)
    {
        $this->swift_Mailer = $swift_Mailer;
        $this->environment = $environment;
    }

    public function sendConfirmationEmail()
    {
        $body = $this->environment->render('email/confirmation.html.twig');
        // send email
        $message = (new \Swift_Message('hello world'))
            ->setFrom('alexanderbyfelice@gmail.com')
            ->setTo('alexander.vdbroeck@gmail.com')
//            ->setTo($user->getEmail())
            ->setBody($body);
        $this->swift_Mailer->send($message);
    }

}
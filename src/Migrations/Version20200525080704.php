<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200525080704 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_product_price (product_id INT NOT NULL, product_price_id INT NOT NULL, INDEX IDX_B17945774584665A (product_id), INDEX IDX_B17945771DA4AD70 (product_price_id), PRIMARY KEY(product_id, product_price_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_product_price ADD CONSTRAINT FK_B17945774584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_price ADD CONSTRAINT FK_B17945771DA4AD70 FOREIGN KEY (product_price_id) REFERENCES product_price (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE product_price_product');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_price_product (product_price_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_6281D0034584665A (product_id), INDEX IDX_6281D0031DA4AD70 (product_price_id), PRIMARY KEY(product_price_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE product_price_product ADD CONSTRAINT FK_6281D0034584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_price_product ADD CONSTRAINT FK_6281D0031DA4AD70 FOREIGN KEY (product_price_id) REFERENCES product_price (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE product_product_price');
    }
}

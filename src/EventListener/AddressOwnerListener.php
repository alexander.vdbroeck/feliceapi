<?php


namespace App\EventListener;


use App\Entity\Address;
use Symfony\Component\Security\Core\Security;

class AddressOwnerListener
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function prePersist(Address $address)
    {
        if ($address->getUser()) {
            return;
        }

        if ($this->security->getUser()) {
            $address->setUser($this->security->getUser());
        }
    }

}





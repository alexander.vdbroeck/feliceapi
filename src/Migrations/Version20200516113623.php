<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516113623 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_price DROP FOREIGN KEY product_price_ibfk_1');
        $this->addSql('DROP INDEX product_id ON product_price');
        $this->addSql('ALTER TABLE product_price DROP product');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_price ADD product INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_price ADD CONSTRAINT product_price_ibfk_1 FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('CREATE INDEX product_id ON product_price (product)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci` COMMENT \'(DC2Type:json)\'');
    }
}

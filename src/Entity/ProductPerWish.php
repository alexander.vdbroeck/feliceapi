<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductPerWish
 *
 * @ORM\Table(name="product_per_wish", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="product_per_wish_wish_list_id_fk", columns={"wish"}), @ORM\Index(name="product_per_wish_product_id_fk", columns={"product"})})
 * @ORM\Entity
 */
class ProductPerWish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \WishList
     *
     * @ORM\ManyToOne(targetEntity="WishList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="wish", referencedColumnName="id")
     * })
     */
    private $wish;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getWish(): ?WishList
    {
        return $this->wish;
    }

    public function setWish(?WishList $wish): self
    {
        $this->wish = $wish;

        return $this;
    }


}

<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ApiResource(
 *     collectionOperations={"post"={"path"="users/confirm"}}
 * )
 *
 */

class UserConfirmation
{
    /**
     * @Assert\NotBlank()
     */
    public $confirmationToken;

}
<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ProductPromotions
 *
 * @ApiResource(
 *
 * )
 * @ORM\Table(name="product_promotions", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProductPromotionsRepository")
 *
 */
class ProductPromotions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"products:collection:get","products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="description", type="string", length=256, nullable=true)
     */
    private $description;

    /**
     * @var int|null
     * @Groups({"products:collection:get","products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="percentage", type="integer", nullable=true)
     */
    private $percentage;

    /**
     * @var int|null
     * @Groups({"products:collection:get","products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="fixed_prise", type="integer", nullable=true)
     */
    private $fixedPrise;

    /**
     * @var \DateTime|null
     * @Groups({"admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="from_date", type="date", nullable=true)
     */
    private $fromDate;

    /**
     * @var \DateTime|null
     * @Groups({"admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="until_date", type="date", nullable=true)
     */
    private $untilDate;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class,inversedBy="ArticlePromotion")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(?int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getFixedPrise(): ?int
    {
        return $this->fixedPrise;
    }

    public function setFixedPrise(?int $fixedPrise): self
    {
        $this->fixedPrise = $fixedPrise;

        return $this;
    }

    public function getFromDate(): ?\DateTimeInterface
    {
        return $this->fromDate;
    }

    public function setFromDate(?\DateTimeInterface $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getUntilDate(): ?\DateTimeInterface
    {
        return $this->untilDate;
    }

    public function setUntilDate(?\DateTimeInterface $untilDate): self
    {
        $this->untilDate = $untilDate;

        return $this;
    }


    public function __toString()
    {
        return  $this->description;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function addProducts(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addArticlePromotion($this);
        }

        return $this;
    }

    public function removeProducts(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            $product->removeArticlePromotion($this);
        }

        return $this;
    }



}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516205925 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_per_order DROP FOREIGN KEY product_per_order_orders_id_fk');
        $this->addSql('ALTER TABLE product_per_order DROP FOREIGN KEY product_per_order_product_id_fk');
        $this->addSql('DROP INDEX product_per_order_product_id_fk ON product_per_order');
        $this->addSql('DROP INDEX product_per_order_orders_id_fk ON product_per_order');
        $this->addSql('ALTER TABLE product_per_order ADD main_order_id INT NOT NULL, ADD product_id INT NOT NULL, DROP product, DROP orders');
        $this->addSql('ALTER TABLE product_per_order ADD CONSTRAINT FK_5525D08C54BD7C4D FOREIGN KEY (main_order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE product_per_order ADD CONSTRAINT FK_5525D08C4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_5525D08C54BD7C4D ON product_per_order (main_order_id)');
        $this->addSql('CREATE INDEX IDX_5525D08C4584665A ON product_per_order (product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_per_order DROP FOREIGN KEY FK_5525D08C54BD7C4D');
        $this->addSql('ALTER TABLE product_per_order DROP FOREIGN KEY FK_5525D08C4584665A');
        $this->addSql('DROP INDEX IDX_5525D08C54BD7C4D ON product_per_order');
        $this->addSql('DROP INDEX IDX_5525D08C4584665A ON product_per_order');
        $this->addSql('ALTER TABLE product_per_order ADD product INT DEFAULT NULL, ADD orders INT DEFAULT NULL, DROP main_order_id, DROP product_id');
        $this->addSql('ALTER TABLE product_per_order ADD CONSTRAINT product_per_order_orders_id_fk FOREIGN KEY (orders) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE product_per_order ADD CONSTRAINT product_per_order_product_id_fk FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('CREATE INDEX product_per_order_product_id_fk ON product_per_order (product)');
        $this->addSql('CREATE INDEX product_per_order_orders_id_fk ON product_per_order (orders)');
    }
}

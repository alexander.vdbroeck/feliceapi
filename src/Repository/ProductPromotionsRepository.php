<?php


namespace App\Repository;


use App\Entity\ProductPromotions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductPromotions|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPromotions|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPromotions[]    findAll()
 * @method ProductPromotions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class ProductPromotionsRepository extends ServiceEntityRepository
{

    /**
     * ProductPromotionsRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductPromotions::class);
    }
}
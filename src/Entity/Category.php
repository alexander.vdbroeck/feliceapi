<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Category
 * @ApiResource(collectionOperations={"get"={"normalization_context"={"groups"={"categorie:collection:get"}}},
 *                                   "post"={"access_control"="is_granted('ROLE_ADMIN')","denormalization_context"={"groups"={"admin:categorie:collection:post"}}}},
 *              itemOperations={"get"={"normalization_context"={"groups"={"categorie:collection:get"}}},
 *                              "put"={"access_control"="is_granted('ROLE_ADMIN')","denormalization_context"={"groups"={"admin:categorie:item:put"}}},
 *                              "delete"={"access_control"="is_granted('ROLE_ADMIN')"}},
 *              attributes={ "pagination_items_per_page"=50}
 *      )
 * @ORM\Table(name="category", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="category_category_id_fk", columns={"parent"})})
 * @ORM\Entity
 * @ApiFilter(SearchFilter::class, properties={ "parent.id": "exact","id" : "exact"})
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 *
 */
class Category
{
    /**
     * @var int
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     * @ORM\Column(name="name", type="string", length=256, nullable=true)
     */
    private $name;

    /**
     * @Vich\UploadableField(mapping="category_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    /**
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @var \Category
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put"})
     * @ORM\ManyToMany(targetEntity=Product::class, mappedBy="categories",cascade={"persist"})
     *
     *
     */
    private $products;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     */
    private $description;





    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        if(!$this->name){
            return "no name";
        }
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return string|null
     * @SerializedName("parentName")
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     */
    public function getParentByName(): ?string
    {
        if(!$this->parent){
            return "no parent";
        }
        return $this->parent->getName();
    }

    /**
     * @return int
     * @SerializedName("parentById")
     * @Groups({"admin:categorie:collection:post","admin:categorie:item:put","categorie:collection:get","products:item:get"})
     */
    public function getParentByIri(): ?int
    {
        if(!$this->parent){
            return 0;
        }
        return (int)$this->parent->getId();
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function addProduct(Product $product): void
    {
        if (!$this->products->contains($product)) {
           return;
        }
        $this->products[] = $product;
        $product->addCategory($this);


//        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function removeProduct(Product $product): void
    {
        if(!$this->products->contains($product)) {
                return;
        }
        $this->products->removeElement($product);
        $product->removeCategory($product);

//        return $this;
    }


    /*  om easy admin toegang te geven aan sub entity's moet en string meegeven worden.*/
    public function __toString()
    {
        if(!$this->name){
            return "no name available";
        }
        return  $this->name;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }



}

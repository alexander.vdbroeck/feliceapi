<?php


namespace App\EventListener;

use App\Entity\Orders;
use Symfony\Component\Security\Core\Security;

class OrderOwnerListener
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function prePersist(Orders $orders)
    {
        // als een user al een bestlling heeft
        if($orders->getUser()){
            return;
        }
        // als het een nieuwe bestelling is worden de juiste adressen opgehaald
        // als er geen leverings adres gevonden wordt, zal de bestelling geannuleerd worden
        $user = $this->security->getUser();
        if($user && $user->getDeliveryAddress()){
            $orders->setUser($this->security->getUser());
            $orders->setDeliveryAddress($user->getDeliveryAddress());
            if($user->getBillingAddress()){
                $orders->setBillingAddress($user->getBillingAddress());            }
        }else{
            return;
        }
    }

}
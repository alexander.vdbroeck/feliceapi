<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;


//### the user can search on product title and category and sort on pirce, or name
//### Subentity's like product price and promotions are filtert in the repositrory using criterea fro better performance  ####

/**
 * Product
 * @ApiResource(collectionOperations={"get"={"normalization_context"={"groups"={"products:collection:get"}}},
 *                                   "post"={"access_control"="is_granted('ROLE_ADMIN')","denormalization_context"={"groups"={"admin:products:collection:post"}}}},
 *              itemOperations={"get"={"normalization_context"={"groups"={"products:item:get"}}},
 *                              "patch"={"access_control"="is_granted('ROLE_ADMIN')","denormalization_context"={"groups"={"admin:products:item:put"}}},
 *                              "delete"={"access_control"="is_granted('ROLE_ADMIN')"}})
 * @ORM\Table(name="product", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={ @ORM\Index(name="parent_id", columns={"parent"})})
 *
 * @ApiFilter(SearchFilter::class, properties={"categories.id": "exact","title":"ipartial"})
 * @ApiFilter(OrderFilter::class, properties={ "title"})
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */

class Product
{
    /**
     * @var int
     * @Groups({"products:collection:get","products:item:get","admin:products:collection:post","admin:products:item:put","categorie:item:get","categorie:collection:get"})
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"products:collection:get","products:item:get","admin:products:collection:post","admin:products:item:put","categorie:item:get","categorie:collection:get"})
     * @ORM\Column(name="title", type="string", length=256, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max= 50
     * )
     *
     */
    private $title;

    /**
     * @var string|null
     * @Groups({"products:item:get","products:collection:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="mini_description", type="string", length=256, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max= 50
     * )
     */
    private $miniDescription;



    /**
     * @var string|null
     * @Groups({"products:item:get","admin:write","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="description", type="string", length=256, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="size", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $size;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="color", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $color;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="weight", type="string", nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $weight;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="weight_unit", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $weightUnit;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="shipment_dimensions", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $shipmentDimensions;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="volume", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $volume;

    /**
     * @var string|null
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put"})
     * @ORM\Column(name="volume_unit", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 10
     * )
     */
    private $volumeUnit;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     *
     *
     * @Groups({"products:item:get","admin:products:collection:post","admin:products:item:put","categorie:collection:get"})
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;



    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="products",cascade={"persist"})
     * @Groups({"admin:products:collection:post","admin:products:item:put","products:item:get"})
     */
    private $categories;



    /**
     * @Groups({"admin:product:item:put","admin:product:collection:post","products:item:get"})
     * @ORM\OneToMany(targetEntity=ProductImage::class, mappedBy="product",cascade={"persist"}, orphanRemoval=true)
     */
    private $images;



    /**
     * @ORM\Column(type="boolean")
     */
    private $active ;

    /**
     * @ORM\ManyToMany(targetEntity=ProductPromotions::class,  mappedBy="products",cascade={"persist"})
     * @Groups({"admin:products:collection:post","admin:products:item:put","categorie:collection:get"})
     */
    public $ArticlePromotion;

    /**
     * @ORM\OneToMany(targetEntity=ProductPrice::class, mappedBy="product", cascade={"persist"}, orphanRemoval=true)
     */
    private $prices;



    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->categories = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
        $this->ArticlePromotion = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->active = false;


    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMiniDescription(): ?string
    {
        return $this->miniDescription;
    }

    public function setMiniDescription(?string $miniDescription): self
    {
        $this->miniDescription = $miniDescription;

        return $this;
    }



    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWeightUnit(): ?string
    {
        return $this->weightUnit;
    }

    public function setWeightUnit(?string $weightUnit): self
    {
        $this->weightUnit = $weightUnit;

        return $this;
    }

    public function getShipmentDimensions(): ?string
    {
        return $this->shipmentDimensions;
    }

    public function setShipmentDimensions(?string $shipmentDimensions): self
    {
        $this->shipmentDimensions = $shipmentDimensions;

        return $this;
    }

    public function getVolume(): ?string
    {
        return $this->volume;
    }

    public function setVolume(?string $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getVolumeUnit(): ?string
    {
        return $this->volumeUnit;
    }

    public function setVolumeUnit(?string $volumeUnit): self
    {
        $this->volumeUnit = $volumeUnit;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
//        if($this->createdAt){
//            return $this->createdAt->format("d-m-Y");
//        }
//        return "not nown";
    }

    /**
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    /**
     * @return $this|null
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * @param Product|null $parent
     * @return $this
     */
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }



    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }


    /**
     * @param Category $category
     * @return $this
     */
    public function addCategory(Category $category): void
    {
        if (!$this->categories->contains($category)) {
            return;
        }
        $this->categories[] = $category;
        $category->addProduct($this);
//        return $this;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function removeCategory(Category $category): void
    {
        if (!$this->categories->contains($category)) {
          return;
        }
        $this->categories->removeElement($category);
        $category->removeProduct($this);
//        return $this;
    }


/**
 * @Groups({"products:collection:get","products:item:get"})
 * @return Collection|ProductImage[]
 */
public function getImages(): Collection
{
    return $this->images;
}

public function addImage(ProductImage $image): self
{
    if (!$this->images->contains($image)) {
        $this->images[] = $image;
        $image->setProduct($this);
    }

    return $this;
}

public function removeImage(ProductImage $image): self
{
    if ($this->images->contains($image)) {
        $this->images->removeElement($image);
        // set the owning side to null (unless already changed)
        if ($image->getProduct() === $this) {
            $image->setProduct(null);
        }
    }

    return $this;
}

    /**
     *
     * @return array
     * @SerializedName("productPrice")
     * @Groups({"products:collection:get","products:item:get"})
     * @throws \Exception
     */
    public function getActivePrice()
    {

        $price =  ProductRepository::getActivePrice($this->prices);
        if($price === "no-price"){
            $this->active = false;
        }
        return $price;

    }



    public function __toString()
    {
        $title =  $this->title ? $this->title: "no -title-";


        return (string) " id: ".$this->id." ".$title;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     *
     * @return array
     * @Groups({"products:item:get","products:collection:get"})
     * @throws \Exception
     */
    public function getArticlePromotionArray():array
    {

        return  ProductRepository::getActivePromotion($this->ArticlePromotion);


    }



    /**
     * @return Collection|ProductPromotions[]
     */
    public function getArticlePromotion():Collection
    {
        return $this->ArticlePromotion;

    }

    /**
     * @param ProductPromotions $articlePromotion
     * @return $this
     */
    public function addArticlePromotion(ProductPromotions $articlePromotion): self
    {
        if (!$this->ArticlePromotion->contains($articlePromotion)) {
            $this->ArticlePromotion[] = $articlePromotion;
        }

        return $this;
    }

    public function removeArticlePromotion(ProductPromotions $articlePromotion): self
    {
        if ($this->ArticlePromotion->contains($articlePromotion)) {
            $this->ArticlePromotion->removeElement($articlePromotion);
        }

        return $this;
    }

/**
 * @return Collection|ProductPrice[]
 */
public function getPrices(): Collection
{
    return ProductRepository::getLastPrices($this->prices);
    return $this->prices;
}

public function addPrice(ProductPrice $price): self
{
    if (!$this->prices->contains($price)) {
        if(!$price->getStartedAt()){
            $price->setStartedAt(new \DateTimeImmutable());
        }
        if(!$price->getEndedAt()){
            $price->setEndedAt(new \DateTimeImmutable("2050-01-01"));
        }

        $this->prices[] = $price;
        $price->setProduct($this);
    }

    return $this;
}

public function removePrice(ProductPrice $price): self
{
    if ($this->prices->contains($price)) {
        $this->prices->removeElement($price);
//         set the owning side to null (unless already changed)
        if ($price->getProduct() === $this) {
            $price->setProduct(null);
        }
    }

    return $this;
}


}

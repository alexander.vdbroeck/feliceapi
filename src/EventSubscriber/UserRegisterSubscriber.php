<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\mailer\MailerServices;
use App\Security\TokenGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegisterSubscriber implements EventSubscriberInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;
    /**
     * @var MailerServices
     */
    private $mailer;


    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGenerator $tokenGenerator,MailerServices $mailer)
    {

        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;

        $this->mailer = $mailer;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return[
            KernelEvents::VIEW => ['userRegistered', EventPriorities::PRE_WRITE]
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function userRegistered(ViewEvent $event){

        /* Voor het wegschrijven van de nieuwe gebruiker:
        -> eerst controlleren of hij een gebruiker is,
        -> paswoord hashen
        -> een token genereren
        -> een email verzenden met de een link die de gebruiker moet volgen.
         */
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if(!$user instanceof User || !in_array($method,[Request::METHOD_POST,Request::METHOD_PUT]) ){
            return;
        }
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user,$user->getPassword())
        );
        // het aanmaken van een confirmatie token
//      $token = $this->tokenGenerator->getRandomSecureToken();
//        $user->setConfirmationToken($token);
//        // email verzenden met token:
//        $this->mailer->sendConfirmationEmail($user);
    }


}
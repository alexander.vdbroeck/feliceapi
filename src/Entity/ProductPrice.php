<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ProductPrice
 * @ApiResource(
 * )
 * @ORM\Table(name="product_price", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})} )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductPriceRepository")
 *
 *
 */
class ProductPrice
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float|null
     * @Groups({"products:item:get","products:collection:get","admin:products:collection:post","admin:products:item:put","admin:products:item:patch"})
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var \DateTime|null
     *@Groups({"admin:products:collection:post","admin:products:item:put","admin:products:item:patch"})
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @var \DateTime|null
     * @Groups({"admin:products:collection:post","admin:products:item:put","admin:products:item:patch"})
     * @ORM\Column(name="ended_at", type="datetime", nullable=true)
     */
    private $endedAt;



    /**
     * @var int|null
     * @Groups({"products:item:get","products:collection:get","admin:products:collection:post","admin:products:item:put","admin:products:item:patch"})
     * @ORM\Column(name="taxrate", type="integer", nullable=false)
     */
    private $taxrate;




    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="prices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;


    public function __construct()
    {
        $this->startedAt = new \DateTimeImmutable();
        $this->endedAt = new \DateTimeImmutable("2050-01-01");
        $this->taxrate = 21;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndedAt(): ?\DateTimeInterface
    {
        return $this->endedAt;
    }

    public function setEndedAt(?\DateTimeInterface $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }


    /**
     * @return int|null
     */
    public function getTaxrate(): ?int
    {

        return $this->taxrate;
    }

    /**
     * @param int|null $taxrate
     */
    public function setTaxrate(?int $taxrate): void
    {
        $this->taxrate = $taxrate;
    }




    public function __toString()
    {
        $until = $this->endedAt ? $this->endedAt->format('d-m-Y'): "no date";
        return (string) $this->price." =>  until=".$until;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

}

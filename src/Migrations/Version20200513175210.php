<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200513175210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address DROP FOREIGN KEY address_user_id_fk');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F818D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_promotions CHANGE product product INT DEFAULT NULL, CHANGE category category INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F818D93D649');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT address_user_id_fk FOREIGN KEY (user) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE product_promotions CHANGE category category INT DEFAULT 0, CHANGE product product INT DEFAULT 0');
    }
}

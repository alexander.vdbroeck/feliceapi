<?php


namespace App\Security;


use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserConfirmationService
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    public function confirmUser(string $confirmationToken)
    {
        // De user opzoeken adhv het token uit de url
        $user = $this->userRepository->findOneBy(['confirmationToken' => $confirmationToken]);
        // Als het token niet gevonden wordt, een error bericht terug sturen
        if(!$user){
            throw new NotFoundHttpException();
        }
        // De user op actief zetten en het token verwijderen.
        $user->setIsEnabled(true);
        $user->setConfirmationToken(null);
        $this->entityManager->flush();
    }

}
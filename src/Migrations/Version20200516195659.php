<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516195659 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category_product MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY category_product_category_id_fk');
        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY category_product_ibfk_1');
        $this->addSql('DROP INDEX id ON category_product');
        $this->addSql('DROP INDEX product_id ON category_product');
        $this->addSql('DROP INDEX category_product_category_id_fk ON category_product');
        $this->addSql('DROP INDEX product ON category_product');
        $this->addSql('ALTER TABLE category_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE category_product ADD category_id INT NOT NULL, ADD product_id INT NOT NULL, DROP id, DROP product, DROP category');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT FK_149244D312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT FK_149244D34584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_149244D312469DE2 ON category_product (category_id)');
        $this->addSql('CREATE INDEX IDX_149244D34584665A ON category_product (product_id)');
        $this->addSql('ALTER TABLE category_product ADD PRIMARY KEY (category_id, product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY FK_149244D312469DE2');
        $this->addSql('ALTER TABLE category_product DROP FOREIGN KEY FK_149244D34584665A');
        $this->addSql('DROP INDEX IDX_149244D312469DE2 ON category_product');
        $this->addSql('DROP INDEX IDX_149244D34584665A ON category_product');
        $this->addSql('ALTER TABLE category_product ADD id INT AUTO_INCREMENT NOT NULL, ADD product INT DEFAULT NULL, ADD category INT DEFAULT NULL, DROP category_id, DROP product_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT category_product_category_id_fk FOREIGN KEY (category) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category_product ADD CONSTRAINT category_product_ibfk_1 FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('CREATE UNIQUE INDEX id ON category_product (id)');
        $this->addSql('CREATE INDEX product_id ON category_product (product)');
        $this->addSql('CREATE INDEX category_product_category_id_fk ON category_product (category)');
        $this->addSql('CREATE INDEX product ON category_product (product)');
    }
}

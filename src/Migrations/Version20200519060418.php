<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200519060418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_image DROP FOREIGN KEY product_image_product_id_fk');
        $this->addSql('DROP INDEX product_image_product_id_fk ON product_image');
        $this->addSql('ALTER TABLE product_image DROP product');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_image ADD product INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_image ADD CONSTRAINT product_image_product_id_fk FOREIGN KEY (product) REFERENCES product (id)');
        $this->addSql('CREATE INDEX product_image_product_id_fk ON product_image (product)');
    }
}

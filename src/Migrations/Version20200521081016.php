<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521081016 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address DROP FOREIGN KEY address_city_id_fk');
        $this->addSql('DROP INDEX address_city_id_fk ON address');
        $this->addSql('ALTER TABLE address DROP city');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234F92F3E70');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE product CHANGE description description VARCHAR(256) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE address ADD city INT DEFAULT NULL');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT address_city_id_fk FOREIGN KEY (city) REFERENCES city (id)');
        $this->addSql('CREATE INDEX address_city_id_fk ON address (city)');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234F92F3E70');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON UPDATE SET NULL ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product CHANGE description description VARCHAR(256) CHARACTER SET latin1 DEFAULT \'geen omschrijving beschikbaar\' COLLATE `latin1_swedish_ci`');
    }
}

<?php


namespace App\Repository;


use App\Entity\ProductPerOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductPerOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPerOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPerOrder[]    findAll()
 * @method ProductPerOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */


class ProductPerOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductPerOrder::class);
    }

}
<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ProductPerOrder
 * @ApiResource(
 *     itemOperations={"get","put"},
 *     collectionOperations={"post","get"}
 * )
 * @ORM\Table(name="product_per_order", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity
 */
class ProductPerOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     * @Groups({"write","read"})
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var bool|null
     * @Groups({"write","read"})
     * @ORM\Column(name="taxrate", type="float", nullable=true)
     */
    private $taxrate;

    /**
     * @var int|null
     * @Groups({"write","read"})
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string|null
     * @Groups({"write","read"})
     * @ORM\Column(name="comment", type="string", length=256, nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=Orders::class, inversedBy="orderDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mainOrder;

    /**
     * @Groups({"write","read"})
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTaxrate(): ?float
    {
        return $this->taxrate;
    }

    public function setTaxrate(?float $taxrate): self
    {
        $this->taxrate = $taxrate;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getMainOrder(): ?Orders
    {
        return $this->mainOrder;
    }

    public function setMainOrder(?Orders $mainOrder): self
    {
        $this->mainOrder = $mainOrder;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }



    public function __toString()
    {
        return (string) $this->product." #".$this->quantity;
    }





}

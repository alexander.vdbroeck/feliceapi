<?php


namespace App\Controller;

use App\Security\UserConfirmationService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     * @return Response
     */
    public function getProducts()
    {
        return new Response("<h1>My Felice homepage</h1>" );

    }

    /**
     *
     * @Route("/confirm-user/{token}")
     * @param string $token
     * @param UserConfirmationService $userConfirmationService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     */
    public function confirmUser(string $token, UserConfirmationService $userConfirmationService)
    {
//        $userConfirmationService->confirmUser($token);
//        return $this->redirect('https://www.felice.surge.sh/confirm');

    }

//    /**
//     * @Route("/app_logout", name="app_logout")
//     * @return Response
//     */
//    public function logout()
//    {
//
//
//        return new Response("<h1>your loged out</h1>" );
//
//    }
//
}
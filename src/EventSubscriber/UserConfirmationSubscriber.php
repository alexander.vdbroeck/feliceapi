<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\UserConfirmation;
use App\Repository\UserRepository;
use App\Security\UserConfirmationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

// classe niet meer in gebruik, deze werd gebruikt om een user te activeren na dat hij de link gevolgd heeft
// die hij per email aangekregen heeft 

class UserConfirmationSubscriber implements EventSubscriberInterface
{


    /**
     * @var UserConfirmationService
     */
    private $userConfirmationService;

    public function __construct(UserConfirmationService $userConfirmationService)
    {

        $this->userConfirmationService = $userConfirmationService;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['confirmationUser', EventPriorities::POST_VALIDATE]
        ];
    }

    public function confirmationUser(ViewEvent $event)
    {
        $request = $event->getRequest();
        if('api_user_confirmations_post_collection' !== $request->get('_route')){
            return;
        }

        /** @var  UserConfirmation $confirmationToken */
        $confirmationToken = $event->getControllerResult();
        $this->userConfirmationService->confirmUser($confirmationToken->confirmationToken);

        $event->setResponse(new JsonResponse(null,Response::HTTP_OK));
    }
}
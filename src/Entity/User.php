<?php

namespace App\Entity;



use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * User
 * @ApiResource(
 *     itemOperations={"get"={"security"="is_granted('ROLE_ADMIN') or object == user"},"patch"={"security"="is_granted('ROLE_ADMIN')", "denormalization_context"={"groups"={"admin:write"}}},
 *     "put"={"security"="is_granted('ROLE_ADMIN') or object == user"}},
 *     collectionOperations={"get"={"security"="is_granted('ROLE_ADMIN')"},"post"={"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"}},
 *      normalizationContext={"groups"={"user:read"}},
 *      denormalizationContext={"groups"={"user:write"}},
 *
 *
 *
 * )
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"}), @ORM\UniqueConstraint(name="email", columns={"email"})})
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @UniqueEntity(fields={"email"})
 *

 *
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"user:read"})
     *
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="gender", type="string", length=30, nullable=true)
     */
    private $gender;

    /**
     * @var string|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="first_name", type="string", length=256, nullable=true)
     * @Assert\Length(
     *     max= 50
     * )
     *
     */
    private $firstName;

    /**
     * @var string|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="second_name", type="string", length=256, nullable=true)
     * @Assert\Length(
     *     max= 50
     * )
     */
    private $secondName;

    /**
     * @var string|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="email", type="string", length=256, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="phoneNr", type="string", length=30, nullable=true)
     * @Assert\Length(
     *     max= 20
     * )
     */
    private $phonenr;

    /**
     * @var \DateTime|null
     * @Groups({"user:read","user:write","admin:write"})
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string|null
     * @Groups({"user:write"})
     * @ORM\Column(name="passw", type="string", length=256, nullable=true)
     *
     */
    private $password;



    /**
     * @Groups({"admin:write"})
     * @ORM\Column(type="simple_array")
     */
    private $roles = [];

    /**
     * @var string|null
     *
     * @ORM\Column(name="regkey", type="string", length=256, nullable=true)
     */
    private $regkey;




    /**
     * @var string|null
     *
     * @ORM\Column(name="external_type", type="string", length=256, nullable=true)
     */
    private $externalType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_id", type="string", length=256, nullable=true)
     */
    private $externalId;

    /**
     * @var bool|null
     * @Groups({"admin:write"})
     * @ORM\Column(name="is_enabled", type="boolean", nullable=true)
     */
    private $isEnabled = true;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="date", nullable=true)
     */
    private $createdAt;



    /**
     * @var \DateTime|null
     * @Groups({"admin:write"})
     * @ORM\Column(name="updated_at", type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     * @Groups({"admin:write"})
     * @ORM\Column(name="deleted_at", type="date", nullable=true)
     */
    private $deletedAt;



    /**
     * @ORM\OneToMany(targetEntity=Address::class, mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     * @Groups({"user:write","admin:write","user:read"})
     */
    private $addresses;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationToken;


    public function __construct()
    {
        $this->roles[] = 'ROLE_USER';
        $this->createdAt = new \DateTimeImmutable();
        $this->addresses = new ArrayCollection();
        $this->isEnabled = true;
    }
    /**
     * @see UserInterface
     */

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @see UserInterface
     */
    public function getFullName()
    {
        return $this->getFirstName(). " ". $this->getSecondName();
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    public function setSecondName(?string $secondName): self
    {
        $this->secondName = $secondName;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }



    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getPhonenr(): ?string
    {
        return $this->phonenr;
    }

    public function setPhonenr(?string $phonenr): self
    {
        $this->phonenr = $phonenr;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * @return string
     * @Groups({"user:read"})
     */
    public function  getAge():string {
        if(!$this->birthday){
            return "no birthday";
        }
        return Carbon::instance($this->birthday)->diffInYears();
    }

    /**
     * @return \DateTimeImmutable|null
     * @Groups({"user:read"})
     */

    public function getBirthday()
    {
        $this->birthday;
//        if($this->birthday){
//            $date = $this->birthday;
//            return $date->format("d-m-Y");
//        }
//        return null;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }





    public function getRegkey(): ?string
    {

        return $this->regkey;
    }

    public function setRegkey(?string $regkey): self
    {
        $this->regkey = $regkey;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getGuest(): ?bool
    {
        return $this->guest;
    }

    public function setGuest(?bool $guest): self
    {
        $this->guest = $guest;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getExternalType(): ?string
    {
        return $this->externalType;
    }

    public function setExternalType(?string $externalType): self
    {
        $this->externalType = $externalType;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(?bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * @return string
     * @SerializedName("memberSince")
     * @Groups("user:read")
     */
    public function getCreatedAtAgo(): string {
        if(!$this->getCreatedAt()){
            return "no date";
        }
        return Carbon::instance($this->getCreatedAt())->diffForHumans();
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTimeInterface|null
     * @Groups({"user:read"})
     */
    public function getUpdatedAt():? DateTimeInterface
    {

        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     * @Groups({"user:read"})
     */

    public function getDeletedAt(): ? DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }





    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
       return $this->roles;

    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return $this
     * @see UserInterface
     */

    public function setPassword(string $password): self
    {
        $this->password = $password;
        $this->updatedAt = new \DateTimeImmutable();
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {

        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }




    /**
     * @return Address|null
     * @Groups({"user:read"})
     */
    public function getBillingAddress()
    {
        if($this->addresses) {
            $criteria = UserRepository::getBillingAddress();
            $adress = $this->addresses->matching($criteria)->toArray();
            if($adress){
                return  $adress[0];
            }
           return null;

        }
        return null ;
    }
    /**
     * @return Address|null
     * @Groups({"user:read"})
     */
    public function getDeliveryAddress()
    {
        if($this->addresses) {
            $criteria = UserRepository::getDeliveryAddress();
            $adress = $this->addresses->matching($criteria)->toArray();
            if($adress){
                return  $adress[0];
            }
            return null;


        }
        return null;
    }


    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setUser($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }

        return $this;
    }

    public function __toString()
    {


        return  $this->email." @id: ".$this->id;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

}

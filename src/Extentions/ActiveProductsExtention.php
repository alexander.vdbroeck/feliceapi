<?php


namespace App\Extentions;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Product;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

class ActiveProductsExtention implements QueryItemExtensionInterface, QueryCollectionExtensionInterface
{

    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {

        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->getActiveProducts($queryBuilder,$resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->getActiveProducts($queryBuilder,$resourceClass);
    }

    public function getActiveProducts(QueryBuilder $queryBuilder, string $resourceClass): void
    {

        // het zelfde princiepe word hier hanteerd als bij de prijzen.
        // als een admin via dit endpoint een request doen, zal hij alle producten te zien krijgen
        // gewone users slechts de actieve producten
        if ($resourceClass !== Product::class) {
            return;
             }
        if($this->security->isGranted("ROLE_ADMIN")){
            return;

        }
        // de query aanpassen zodoende enkel actieve producten gevonden worden
        $rootAlias = $queryBuilder->getRootAlias()[0];
        $queryBuilder->andWhere(sprintf('%s.active = 1', $rootAlias));


    }
}